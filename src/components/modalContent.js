/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-hooks/rules-of-hooks */
import React, { useState, useEffect } from 'react';
import * as config from '../config/const'
import DatePicker from 'react-date-picker';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import  { addPointsValidate } from '../config/schema';
import { toast } from 'react-toastify';
import Paper from '@material-ui/core/Paper';
import dayJs from 'dayjs';
// Table
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
// end

const useStyles = makeStyles(theme => ({
    button: {
      margin: theme.spacing(1),
    },
    input: {
      display: 'none',
    },
    root: {
        width: '100%',
      },
      paper: {
        marginTop: theme.spacing(0),
        width: '100%',
        overflowX: 'auto',
        marginBottom: theme.spacing(1),
      },
    table: {
        maxWidth: "100%",
      },
  }));

function modalContent({ id, data, selectOptions, AddPointsAPICall, pointsList, deletePoints, onFileChange, onFileUploadClickHandler }) {
    
    const classes = useStyles();
    // console.log('Component',pointsList, data)
    
    const [content, setContent] = useState(null)
    const [contentId, setContentId] = useState(null)
    const [rewardDate, setRewardDate] = useState(new Date());
    // const [optionsSelect, setOptionsSelect] = useState([])
    const [selectedOption, setSelectedOption] = useState(0);
    
    const onButtonSubmit = () => {
        const temp = {
            eid : data.eid,
            cid: parseInt(selectedOption),
            reward_date: dayJs(rewardDate).format('YYYY-MM-DD'),
            createdBy: 12
        }
        
        let valid = addPointsValidate(temp);
        if(valid){
            AddPointsAPICall(temp);
        }else {
        console.log(addPointsValidate.errors)
        addPointsValidate.errors.map( v =>
            toast.error(`${v.dataPath}-${config.ERROR_MESSAGE}`)
        )
        
        }
    }
    const selectChange = e => {
        
        setSelectedOption(e.target.value)
    }

    const dateChange = date => {
        console.log(date)
        setRewardDate(date)
    }
    
    if(contentId !== id ){
        setContentId(id)
    }
    
    useEffect(() => {
        switch (id) {
            case config.ADD_POINT:
                setContent(<div className="add-point">
                    <h3>Add Points</h3>
                    <div>Name: {data.name}</div>
                    {/* <div>Selected Option: {selectedOption}</div> */}
                    <br></br>
                    <div>Category Type:</div>
                    <select onChange={e => selectChange(e)}>
                        <option value={0} >Select Category</option>
                            {selectOptions && selectOptions.map( v => {
                                return <React.Fragment>
                                    <option value={parseInt(v.cid)} >{v.name}</option>
                                </React.Fragment>
                            })}
                    </select>
                    <br/>
                    <br/>
                    <div>
                    <div>Date: </div>
                    <DatePicker
                        onChange={dateChange}
                        value={rewardDate}
                    />
                    </div>
                    <br/>
                    <Button variant="contained" color="primary" className={classes.button} onClick={onButtonSubmit}>SUBMIT</Button>
                </div>)
                setContentId(id)
                break;
            case config.SHOW_DETAILS:
                    setContent(
                        <div className="table-overflow">
                            
                            {/* <div className={classes.root}> */}
                            <Paper className={classes.paper}>
                            <h3>Points Details</h3>
                            <h4>NAME: {data.name}</h4>
                            <Table className={classes.table} size="small">
                                <TableHead>
                                <TableRow>
                                    <TableCell>Reward ID</TableCell>
                                    <TableCell align="center">Category</TableCell>
                                    <TableCell align="center">Points</TableCell>
                                    <TableCell align="center">Delete Record</TableCell>
                                </TableRow>
                                </TableHead>
                                <TableBody>
                                {pointsList && pointsList.map(row => (
                                    <TableRow>
                                    <TableCell component="th" scope="row">
                                        {row.rid}
                                    </TableCell>
                                    <TableCell align="center">{row.category}</TableCell>
                                    <TableCell align="center">{row.point}</TableCell>
                                    <TableCell align="center"><Button color="secondary" className={classes.button} onClick={()=> deletePoints(row.rid)}>DELETE</Button></TableCell>
                                    </TableRow>
                                ))}
                                </TableBody>
                            </Table>
                            <br/>
                            <div>TOTAL POINTS: {pointsList && pointsList.reduce((acc,v) => {return acc + v.point},0)}</div>
                            <br/>
                            </Paper>
                        </div>
                    )
                    break;
            case config.FILE_UPLOAD:
                setContent(
                <div className="file-upload">
                    <h3>FILE UPLOAD</h3>
                    <input type="file" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" onChange={e => onFileChange(e)}/>
                    <Button variant="contained" component="span" className={classes.button} onClick={e => onFileUploadClickHandler(e)} >Upload</Button>
                </div>
                )
                break;
            default:
                setContent(
                    <div>Loading......</div>
                )
                setContentId(id)
        }
        return () => {
            setContent(null)
        };
    }, [rewardDate,selectedOption,contentId,pointsList])

    
    return <div>{content}</div>
    
}

export default modalContent;