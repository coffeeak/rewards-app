import React, { useState, useEffect } from 'react';
import Modal from 'react-modal';
 
const customStyles = {
  content : {
    top                   : '50%',
    left                  : '50%',
    right                 : 'auto',
    bottom                : 'auto',
    marginRight           : '-50%',
    transform             : 'translate(-50%, -50%)'
  }
};
 
function CustomModal ({ open }) {
   const  [modalOpen, setModalOpen] = useState(false)

   useEffect(() => {
       setModalOpen(open)
   }, [open])

   function closeModal () {
       setModalOpen(false)
   }
   return <div>
       <Modal 
        isOpen={modalOpen}
        contentLabel="onRequestClose Example"
        onRequestClose={closeModal}
        style={customStyles}
       >
           <div>lol</div>
       </Modal>
   </div>
}

export default CustomModal