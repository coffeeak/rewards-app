import  Ajv from 'ajv';

const  ajv = new Ajv();

export const addPointSchema = {
    "type" : "object",
    "required" : ["eid", "cid", "createdBy", "reward_date"],
    "properties" : {
        
        "eid": { "type": ["number", "string"] },
        "cid":{ "type": "integer", "minimum": 1 },
        "createdBy":{ "type": ["number", "string"] },
        "reward_date" : {"type":"string"}
    }
}

export const  addPointsValidate = ajv.compile(addPointSchema);