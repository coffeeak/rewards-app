// API 
export const BASE_API = 'http://3.84.112.206:8000/'
export const FILE_UPLOAD_API = `${BASE_API}api/v1/file/upload`
export const GET_EMPLOYEE_API = `${BASE_API}/api/v1/employees`
export const GET_CATEGORIES_API = `${BASE_API}api/v1/categories`
export const ADD_POINTS_API = `${BASE_API}api/v1/add/points`
export const DELETE_POINTS_API = id => `${BASE_API}api/v1/delete/points?rid=${id}`
export const REWARD_LIST_API = id =>`${BASE_API}api/v1/rewards/list?eid=${id}`
// END

// Consts

export const ADD_POINT = "ADD_POINT";
export const SHOW_DETAILS = "SHOW_DETAILS"
export const FILE_UPLOAD = "FILE_UPLOAD"
export const ERROR_MESSAGE = 'Please select/enter proper value'
export const PAGE_WIDTH = 440
