import React, { useState, useEffect } from 'react';
import * as config from './config/const'
import { Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import axios from 'axios';
import  ModalContent  from './components/modalContent'
import { css } from '@emotion/core';
import { HashLoader } from 'react-spinners';
// Table
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
// end
// Modal
import Modal from '@material-ui/core/Modal';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
 import CustomModal from './components/modal';
  
import {GET_EMPLOYEE_API, GET_CATEGORIES_API, ADD_POINTS_API, DELETE_POINTS_API, REWARD_LIST_API, FILE_UPLOAD_API} from './config/const'

import './App.css';

var tempFile = null 

toast.configure();

const override = css`
    display: block;
    position: absolute;
    width: 100vw;
    height:100vh;
    border-color: red;
    z-index:9999;
`;

function getWindowDimensions() {
  const { innerWidth: width, innerHeight: height } = window;
  return {
    width,
    height
  };
}

function useWindowDimensions() {
  const [windowDimensions, setWindowDimensions] = useState(
    getWindowDimensions()
  );

  useEffect(() => {
    function handleResize() {
      setWindowDimensions(getWindowDimensions());
    }

    window.addEventListener("resize", handleResize);
    return () => window.removeEventListener("resize", handleResize);
  }, []);

  return windowDimensions;
}


function getModalStyle() {

  return {
    top: "30%",
    left: "50%",
    msTransform: `translate(-${50}%, -${50}%)`,
    "-webkit-transform": `translate(-${50}%, -${50}%)`,
    transform: "translate(-50%,-50%)",
  };
}


function App() {
  const { width } = useWindowDimensions();

  const useStyles = makeStyles(theme => ({
    root: {
      // flexGrow: 1,
      width: "100%",
    },
    // paper: {
    //   padding: theme.spacing(2),
    //   textAlign: 'center',
    //   color: theme.palette.text.secondary,
    // },
    paper: {
      position: 'absolute',
      alignItems: "center",
      display: "flex",
      justifyContent: "center",
      width: width > config.PAGE_WIDTH ? "45%": "90%",
      margin:"auto",
      overflowX:"auto",
      backgroundColor: theme.palette.background.paper,
      // border: '2px solid #000',
      // boxShadow: theme.shadows[5],
      padding: theme.spacing(2, 4, 3),
      textAlign:"center"
    },
    table: {
      tableLayout:"fixed",
      width:"95vw",
    },
    button: {
      margin: theme.spacing(1),
    },
    input: {
      display: 'none',
    },
  }));

  const classes = useStyles();

  const [loading, setLoading] = useState(true)
  const [empPoint, setEmpPoint] = useState({
    eid: null,
    cid: null,
    reward_date: null
  })

  const [modalStyle] = React.useState(getModalStyle);
  const [modal, setModal] = React.useState(false);
  const [modalType, setModalType] = useState(null);
  const [uploadFile, setUploadFile] = useState(null);
  const [ empList, setEmpList] = useState([
    { 
      "eid": 7,
      "name": "Prajna",
      "email": "prajna@coffeebeans.io",
      "role": "",
      "designation": "Software Engineer",
      "points": 10
    },
    {
      "eid": 1,
      "name": "akram",
      "email": "akram@coffeebeans.io",
      "role": "",
      "designation": "Software Engineer",
      "points": 20

    }
  ])

  const [categories, setCategories] =  useState([
    {
        "cid": 1,
        "name": "Marketing",
        "point": 15,
        "subCategory": null

    },
    {
      "cid": 2,
      "name": "Arrange Meet-ups",
      "point": 50,
      "subCategory": null

    }
  ])

  const [pointsList, setPointList] = useState([
  ])


  const getEmployees = async () => {
    try{
      setLoading(true)
      const { data }  = await axios.get(GET_EMPLOYEE_API)
      setEmpList(data)
    }catch(e){
      toast.error(e.message);
    }
    setLoading(false);
  }

  const getCategories = async ()=> {
    try{
      const { data }  = await axios.get(GET_CATEGORIES_API)
      setCategories(data)
    }catch(e){
      toast.error(e.message);
    }
  }

  const setModalUp = (id,data) => {
    
    if(id === config.SHOW_DETAILS){
      getPointListOfEmp(data.eid)
    }
    setEmpPoint({
      eid: data.eid,
      name : data.name,
      reward_date: new Date(),

    })
    setModal(true);
    setModalType(id);
  }

  // API CALLS 
  const AddPointsAPICall = async submitData => {
    
    try {
      await axios.post(ADD_POINTS_API, submitData);
      getEmployees();
      toast.success("Successfully added points")
      modalClose();
    } catch (error) {
      toast.error(error.message)
    }
  }
  // END

  function modalClose(){
    setModal(false);
    setEmpPoint({
      eid: null,
      cid: null,
      reward_date: null
    });
    setPointList([]);
    tempFile = null;
  };

  useEffect(() => {
    let online = navigator.onLine ? "online" : "offline";
    if(online){
      getEmployees()
      getCategories()
    }
    return () => {
      setEmpList([])
    };
  }, [])

  // var condition = navigator.onLine ? "online" : "offline";
  
  
  async function  deletePoints (id) {
    try {
      await axios.post(DELETE_POINTS_API(id),{});
      toast.success("Successfully deleted points");
      modalClose();
      getEmployees();
    } catch (e) {
      toast.error(e.message)
    }
}

async function getPointListOfEmp (eid) {
  try {
    setLoading(true)
    const {data} = await axios.get(REWARD_LIST_API(eid))
    setPointList(data)
  } catch (e) {
    toast.error(e.message)
  }
  setLoading(false)
}

async function onFileChangeHandle (e) {
  e.preventDefault();
  setUploadFile(e.target.files[0])
  tempFile = e.target.files[0];
}

const  onFileUploadClickHandler  = async e => {
  e.preventDefault();
    try {
      setLoading(true);
      if(tempFile){
        const data = new FormData();
        data.append('file', tempFile);
        await axios.post(FILE_UPLOAD_API, data)
        getEmployees()
        setModal(false);
      }else {
        toast.error("Please select XSLX file")
      }
      
    } catch (e) {
      toast.error(e.message)
    }
    setLoading(false);
}

  return (
    <div className={classes.root}>
      <Grid container>
      <div className='sweet-loading'>
        <HashLoader
          css={override}
          sizeUnit={"px"}
          size={100}
          color={'#008080'}
          loading={loading}
        />
      </div>
      <nav id="navbar">
          <h1 className="logo">
            <span className="text-primary">
              <i className="fas fa-book-open"></i> Rewards</span></h1>
          <ul>
            {/* <li><Button variant="contained" color="primary" className={classes.button}>UPLOAD</Button></li> */}
            <li> 
              {/* <input
                accept="image/*"
                className={classes.input}
                id="contained-button-file"
                multiple
                type="file"
                onChange={onFileChangeHandle}
              /> */}
              <label htmlFor="contained-button-file">
              <Button variant="contained" component="span" className={classes.button} onClick={()=> setModalUp(config.FILE_UPLOAD,{})}>
                Update
              </Button>
            </label>
            </li>
          </ul>
      </nav>
        {/* <Grid item xs={12}>
          <div className="header">
            <div style={{textAlign:"center"}}>REWARDS</div>
            <div className="upload-btn">
            </div>
            </div>
        </Grid> */}
        <Grid item xs={12}>
        {
          empList && 
            <div className="empInfo">
              
              <Table className={classes.table}>
                <TableHead>
                  <TableRow>
                    <TableCell>Employee ID</TableCell>
                    <TableCell align="right">Name</TableCell>
                    { width > config.PAGE_WIDTH && <TableCell align="right">Designation</TableCell> }
                    <TableCell align="right">Points</TableCell>
                    <TableCell align="right">Add Points</TableCell>
                    <TableCell align="right">Point Details</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {empList && empList.map(row => (
                    <TableRow key={row.name}>
                      <TableCell component="th" scope="row">
                        {row.eid}
                      </TableCell>
                      <TableCell align="right">{row.name}</TableCell>
                      { width > config.PAGE_WIDTH && <TableCell align="right">{row.designation}</TableCell> }
                      <TableCell align="right">{row.points}</TableCell>
                      <TableCell align="right"><Button color="inherit" className={classes.button} onClick={()=> setModalUp(config.ADD_POINT,row)} >ADD</Button></TableCell>
                      <TableCell align="right"><Button color="primary" className={classes.button} onClick={()=> setModalUp(config.SHOW_DETAILS,row)}>VIEW</Button></TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </div>
          
        }
        </Grid>
      </Grid> 
      <Modal
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
        open={modal}
        onClose={modalClose}
      >
          <div style={modalStyle} className={classes.paper}>
          <ModalContent 
            id={modalType}
            data={empPoint} 
            selectOptions={categories} 
            AddPointsAPICall={AddPointsAPICall} 
            pointsList={pointsList} 
            deletePoints={deletePoints}
            onFileChange={onFileChangeHandle}
            onFileUploadClickHandler={onFileUploadClickHandler}
          />
        </div>
      </Modal>
      <CustomModal open={false}
      ></CustomModal> 
    </div>
  );
}

export default App;
